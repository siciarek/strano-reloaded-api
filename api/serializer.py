from django.contrib.auth.models import User, Group
from decimal import Decimal

from django.contrib.auth.models import User, Group
from rest_framework.serializers import (
    HyperlinkedModelSerializer,
    ModelSerializer,
    SerializerMethodField,
)
from rest_framework.validators import (
    UniqueTogetherValidator,
)

import app.models


class GroupSerializer(ModelSerializer):
    class Meta:
        model = Group
        fields = (
            'id',
            'name',
        )


class BasicUserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'first_name',
            'last_name',
            'email',
        )


class UserSerializer(ModelSerializer):
    groups = GroupSerializer(required=True, many=True)
    permissions = SerializerMethodField()

    def get_permissions(self, instance):
        return instance.get_all_permissions()

    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'first_name',
            'last_name',
            'email',
            'is_authenticated',
            'is_superuser',
            'is_active',
            'is_staff',
            'groups',
            'permissions',
        )


class ProjectTemplateSerializer(ModelSerializer):
    class Meta:
        model = app.models.ProjectTemplate
        fields = '__all__'


class TaskSerializer(ModelSerializer):
    run_by = BasicUserSerializer()
    duration = SerializerMethodField()

    def get_duration(self, instance):
        return f'{instance.modified - instance.created}'.split(".")[0]

    class Meta:
        model = app.models.Task
        fields = (
            "id",
            "duration",
            "status",
            "pid",
            "project",
            "stage",
            "working_directory",
            "command",
            "run_by",
            "created",
            "modified",
            "description",
            "action",
            "revision",
            "data",
        )


class ProjectConfigurationValueSerializer(ModelSerializer):
    class Meta:
        model = app.models.ProjectConfigurationValue
        fields = '__all__'


class HostSerializer(ModelSerializer):
    class Meta:
        model = app.models.Host
        fields = '__all__'


class ProjectSerializer(ModelSerializer):
    class Meta:
        model = app.models.Project
        fields = (
            "id",
            "created",
            "modified",
            "name",
            "description",
            "directory_name",
            "hooks",
            "template",
        )


class StageSerializer(ModelSerializer):
    class Meta:
        model = app.models.Stage
        fields = (
            "id",
            "created",
            "modified",
            "name",
            "description",
            "hooks",
            "notification_emails",
            "project",
        )


class RecipeSerializer(ModelSerializer):
    creator = BasicUserSerializer()

    class Meta:
        model = app.models.Recipe
        fields = (
            "id",
            "name",
            "description",
            "body",
            "creator",
        )


class StageRecipetSerializer(ModelSerializer):
    stage = StageSerializer()
    recipe = RecipeSerializer()

    class Meta:
        model = app.models.StageRecipe
        fields = '__all__'


class StageHostRoleSerializer(ModelSerializer):
    stage = StageSerializer()
    host = HostSerializer()

    class Meta:
        model = app.models.StageHostRole
        fields = '__all__'
