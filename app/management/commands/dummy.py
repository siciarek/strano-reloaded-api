from django.core.management.base import BaseCommand
from django.conf import settings
from app.container import Ioc
from decimal import Decimal
import re


class Command(BaseCommand):
    help = 'Dummy command for test purposes.'

    def add_arguments(self, parser):
        parser.add_argument(
            '-s',
            '--source-keeper-id',
            help='Source keeper ID.',
            required=False,
            default=7,
        )
        parser.add_argument(
            '-f',
            '--file',
            help='Action to run.',
            required=True,
        )
        parser.add_argument(
            '-r',
            '--run',
            action='store_true',
            required=False,
            default=False,
            help='If you use this the real Google API request will be performed.',
        )

    def handle(self, *args, **options):
        pass
