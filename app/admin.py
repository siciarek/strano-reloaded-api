from django.contrib import admin
from django.contrib.auth.models import User as AuthUser
from simple_history.admin import SimpleHistoryAdmin
from django.utils.translation import ugettext_lazy as _
from app.models import (
    SystemVariable,
    Task,
    StageRecipe,
    Recipe,
    Host,
    StageHostRole,
    ProjectConfigurationValue,
    ProjectTemplate,
    Project,
    Stage,
)

EMPTY_VALUE_DISPLAY = '—'

admin.site.site_title = "Strano Reloaded"
admin.site.site_header = "Strano Reloaded"
admin.site.index_title = _('System Management')


def get_full_name(self):
    return f'{self.first_name} {self.last_name}' if self.first_name and self.last_name else self.username


AuthUser.add_to_class("__str__", get_full_name)


class AdminCssMixin(object):
    class Media:
        css = {
            'all': ('css/admin.css',),
        }


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = (
        '__str__',
        'action',
        'status',
        'project',
        'stage',
        'run_by',
        'description',
    )

    def has_change_permission(self, request, obj=None):
        return False


@admin.register(Host)
class HostAdmin(admin.ModelAdmin):
    pass


@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = (
        '__str__',
        'description',
    )
    fields = (
        'name',
        'body',
        'description',
    )


@admin.register(StageRecipe)
class StageRecipeAdmin(admin.ModelAdmin):
    list_display = (
        '__str__',
    )
    fields = (
        'stage',
        'recipe',
    )


@admin.register(StageHostRole)
class StageHostRoleAdmin(admin.ModelAdmin):
    list_display = (
        '__str__',
        'ssh_port',
        'primary',
    )


@admin.register(ProjectTemplate)
class ProjectTemplateAdmin(admin.ModelAdmin):
    list_display = (
        '__str__',
        'dir_template',
        'created',
        'modified',
    )


@admin.register(ProjectConfigurationValue)
class ProjectConfigurationValueAdmin(admin.ModelAdmin):
    list_display = (
        '__str__',
        'value',
        'prompt_on_deploy',
    )
    fields = (
        'project',
        'name',
        'value',
        'prompt_on_deploy',
    )


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = (
        '__str__',
        'directory_name',
        'template',
        'created',
        'modified',
    )
    fields = (
        'name',
        'directory_name',
        'template',
        'description',
        'hooks',
    )
    readonly_fields = (
        'directory_name',
    )


@admin.register(SystemVariable)
class SystemVariableAdmin(admin.ModelAdmin):
    list_display = (
        '__str__',
        'project',
        'stage',
        'name',
        'value',
        'created',
        'modified',
    )


@admin.register(Stage)
class StageAdmin(admin.ModelAdmin):
    list_display = (
        '__str__',
        'created',
        'modified',
    )
    fields = (
        'project',
        'name',
        'notification_emails',
        'description',
        'hooks',
    )
