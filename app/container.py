import logging

from dependency_injector import containers, providers
from django.conf import settings
from app.services import (
    MongoDbClient,
    TaskRunner,
    ActiveMqClient,
)


class Ioc(containers.DeclarativeContainer):
    """Definition of Services"""

    logger = logging.getLogger(settings.APP_ENV)

    active_mq_client = providers.Singleton(
        ActiveMqClient,
        logger=logger,
        config=settings.QUEUE_CONF,
        default_queue='strano/task',
        disconnect_delay_secs=2
    )

    task_runner = providers.Singleton(
        TaskRunner,
        logger=logger,
        project_root=settings.PROJECT_ROOT,
        template_root=settings.TEMPLATE_ROOT,
        queue_client=active_mq_client,
        docker_compose_bin='/usr/local/bin/docker-compose',
    )
