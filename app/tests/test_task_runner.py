from django.test import TransactionTestCase
from unittest_data_provider import data_provider
from unittest import mock

from app.container import Ioc
from app.services import TaskRunner


class TaskRunnerTestCase(TransactionTestCase):
    srv: TaskRunner

    def setUp(self) -> None:
        self.srv = Ioc.task_runner()

    def test_container_check(self):
        self.assertEqual(type(self.srv), TaskRunner)

    @data_provider(lambda: (
            ("659c58251fa8a1160b6047f3431cfbd9fc77da15\tHEAD", "659c58251fa8a1160b6047f3431cfbd9fc77da15",),
            ("      01 659c58251fa8a1160b6047f3431cfbd9fc77da15\tHEAD", "659c58251fa8a1160b6047f3431cfbd9fc77da15",),
            ("      659c58251fa8a1160b6047f3431cfbd9fc77da15\tHEAD", "659c58251fa8a1160b6047f3431cfbd9fc77da15",),
            ("659c58251fa8a1160b6047f3431cfbd9fc77da15", None,),
            ("659c58251fa8a1160b6047f3431cfbd9fc77da15\tFOOT", None,),
            ("51fa8a1160b6047f3431cfbd9fc77da15\tHEAD", None,),
    ))
    def test_fetch_revision(self, text, expected):
        actual = self.srv.fetch_revision(text=text)
        self.assertEqual(actual, expected)
