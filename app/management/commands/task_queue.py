from django.core.management.base import BaseCommand

from app.container import Ioc


class Command(BaseCommand):
    help = 'Task queue daemon.'

    def __init__(self, stdout=None, stderr=None, no_color=False, force_color=False):
        super().__init__(stdout=None, stderr=None, no_color=False, force_color=False)
        self.queue = Ioc.active_mq_client()
        self.runner = Ioc.task_runner()

    def add_arguments(self, parser):
        parser.add_argument(
            '-d',
            '--daemon',
            action='store_true',
            required=False,
            default=False,
            help='Run in daemon mode (infinite loop).',
        )

    def process(self):
        for data in self.queue.read():
            self.runner.set_output_handler(self).run(data=data)

    def handle(self, *args, **options):
        daemon_mode = options.get('daemon')
        self.stdout.write(self.style.SUCCESS(
            f'TASK QUEUE ({"" if daemon_mode else "NON "}DAEMON MODE)'
        ))

        if daemon_mode:
            while True:
                self.process()
        else:
            self.process()
