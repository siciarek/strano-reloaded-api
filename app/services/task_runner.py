from app.models import Project, Stage, Task
import re
import os
import glob
import shutil
from datetime import datetime
from django.contrib.auth.models import User as AuthUser
from app.models import Task
import subprocess
from datetime import datetime


class TaskRunner:
    REVISION_RX = r'^\s*(?:\d+\s+)?([\da-f]{40})\s+HEAD$'
    EXCEPTION_RX = r'.*?terminated with exception.*?|^cap aborted!$'

    project: Project
    stage: Stage
    project_root: str
    docker_compose_bin: str
    template_root: str
    output_handler: object

    def __init__(self, logger, docker_compose_bin, project_root, template_root, queue_client, ):
        if os.path.isdir(project_root) is False:
            raise FileNotFoundError(f'Invalid project root directory: "{project_root}".')

        if os.path.isdir(template_root) is False:
            raise FileNotFoundError(f'Invalid template root directory: "{template_root}".')

        self.logger = logger
        self.project_root = project_root
        self.template_root = template_root
        self.queue_client = queue_client
        self.docker_compose_bin = docker_compose_bin

    def fetch_revision(self, text):
        match = re.match(self.REVISION_RX, text)
        return None if match is None else match[1]

    def set_output_handler(self, handler: object):
        self.output_handler = handler
        return self

    def log(self, message, log_type: str = 'info'):

        if log_type == 'error':
            self.logger.error(message)
        elif log_type == 'warning':
            self.logger.warning(message)
        elif log_type in ('info', 'success',):
            self.logger.info(message)
        else:
            raise Exception(f'Type "{log_type}" is not supported.')

        if self.output_handler is not None:
            styles = {
                'error': self.output_handler.style.ERROR,
                'warning': self.output_handler.style.WARNING,
                'success': self.output_handler.style.SUCCESS,
                'info': lambda x: x,
            }
            message = styles.get(log_type)(
                f'{log_type.upper():8s} [{datetime.now().strftime("%Y-%m-%d %H:%M:%S")}] task_runner : {message}'
            )

            self.output_handler.stdout.write(message)

    def create(self, project: Project, stage: Stage, action: str, capistrano_task: str, creator: AuthUser):

        self.__validate_action(action=action)

        project_directory = self.__prepare_stage_directory(
            project=project,
            stage=stage,
        )

        command = self.__generate_command(
            project=project,
            stage=stage,
            action=action,
            capistrano_task=capistrano_task,
        )

        description = f'Action {action} at {datetime.now().strftime("%Y-%m-%d %H:%M:%S")}'

        task = Task(
            project=project,
            stage=stage,
            run_by=creator,
            description=description,
            action=action,
            command=command,
            working_directory=project_directory,
            data=[],
        )

        if action in Task.ACTIONS:
            task.save()
            self.queue_client.write(data=dict(id=task.id))

        return task

    def run(self, data: dict):

        task: Task = Task.objects.get(
            status=Task.STATUS_PENDING,
            id=data.get('id'),
        )

        self.log(f'START PROCESSING : "{task}" command="{task.command}", cwd="{task.working_directory}"')

        proc = subprocess.Popen(
            args=task.command.split(' '),
            cwd=task.working_directory,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            stdin=subprocess.PIPE,
        )

        task.pid = proc.pid
        task.status = Task.STATUS_RUNNING
        task.save()

        return_code = None
        error_occurred = False

        while data := proc.stdout.readline():

            task.refresh_from_db()

            line = data.decode("utf-8").rstrip()

            if re.match(self.EXCEPTION_RX, line) is not None:
                error_occurred = True

            if task.revision is None:
                task.revision = self.fetch_revision(text=line)

            task.data.append(line)

            self.log(message=line)

            if task.status == Task.STATUS_CANCELED:
                proc.kill()
                task.data.append(f'{datetime.now():%Y-%m-%d %H:%M:%S} {Task.STATUS_CANCELED.upper()}')
                self.log(
                    message=f'CANCELED : "{task}" "{task.command}", status={task.status}',
                    log_type='warning',
                )
                task.save()
                return

            task.save()
            return_code = proc.returncode

        if error_occurred is True or return_code is not None:
            task.status = Task.STATUS_FAILED
            message = f'FAILED : "{task}" "{task.command}", ' \
                      f'return_code={return_code}/{error_occurred}'
            log_type = 'error'
        else:
            task.status = Task.STATUS_FULFILLED
            message = f'SUCCEED : "{task}" "{task.command}"'
            log_type = 'success'

        self.log(message=message, log_type=log_type)
        task.save()

    def __prepare_stage_directory(self, project: Project, stage: Stage):

        # 1. Create project directory (if exists remove it first).

        project_directory = os.path.join(self.project_root, project.directory_name)
        if os.path.isdir(project_directory) is True:
            shutil.rmtree(project_directory)

        # 2. Copy all the template data

        common_template_directory = os.path.join(self.template_root, 'common')
        if not os.path.isdir(common_template_directory):
            raise FileNotFoundError(f'Invalid template directory: "{common_template_directory}".')

        template_directory = os.path.join(self.template_root, project.template.dir_template)
        if not os.path.isdir(template_directory):
            raise FileNotFoundError(f'Invalid template directory: "{template_directory}".')

        shutil.copytree(common_template_directory, project_directory)

        for file_name in glob.glob(os.path.join(template_directory, '*')):
            shutil.copy(src=file_name, dst=project_directory)

        for file_name in glob.glob(os.path.join(template_directory, '.*')):
            shutil.copy(src=file_name, dst=project_directory)

        # 3. Generate and update files from project and stage data

        tasks_directory = os.path.join(project_directory, 'lib', 'capistrano', 'tasks')
        if not os.path.isdir(tasks_directory):
            raise FileNotFoundError(f'Invalid tasks directory: "{tasks_directory}".')

        for r in stage.stage_recipes.all():
            file_name = os.path.join(tasks_directory, f'{r.recipe.name}.rake')
            with open(file=file_name, mode='w') as file:
                file.write(r.recipe.body)

        capfile_path = os.path.join(project_directory, 'Capfile')
        if not os.path.isfile(capfile_path):
            raise FileNotFoundError(f'Invalid Capfile: "{capfile_path}".')

        project_hooks = project.hooks.splitlines()
        stage_hooks = stage.hooks.splitlines()
        hooks = "\n".join(stage_hooks) if len(stage_hooks) > 0 else "\n".join(project_hooks)

        with open(file=capfile_path, mode='a') as file:
            file.write(hooks)

        project_remote_user = project.system_variables.get(name='REMOTE_USER')
        stage_remote_user = stage.system_variables.get(name='REMOTE_USER')
        user = stage_remote_user.value if stage_remote_user else project_remote_user.value

        servers = {}

        stage_template = r'server "{}", user: "{}", roles: %w{{{}}}'

        for role in stage.stages.all():
            host = role.host.address
            if host not in servers:
                servers[host] = []

            role = role.role if role.role else role.custom_role
            servers[host].append(role)

        server_roles = []
        for server, roles in servers.items():
            server_roles.append(stage_template.format(server, user, " ".join(roles)))

        stage_path = os.path.join(project_directory, 'config', 'deploy', f'{stage.name}.rb')
        with open(file=stage_path, mode='w') as file:
            file.write("\n".join(server_roles))

        shutil.move(
            src=os.path.join(project_directory, 'deploy.rb'),
            dst=os.path.join(project_directory, 'config', 'deploy.rb'),
        )

        return project_directory

    def __generate_command(self, project: Project, stage: Stage, action, capistrano_task: str = None):
        self.__validate_action(action=action)

        if action == Task.ACTION_EXECUTE and (capistrano_task is None or capistrano_task.strip() == ''):
            raise Exception('No valid action provided.')

        actions = {
            Task.ACTION_BUILD: 'up --build --remove-orphans',
            Task.ACTION_DEPLOY: 'deploy',
            Task.ACTION_CHECK: 'deploy:check',
            Task.ACTION_SETUP: 'deploy:check',
            Task.ACTION_INFO: '--tasks',
            Task.ACTION_EXECUTE: f'{capistrano_task}',
        }

        command = [self.docker_compose_bin]

        if action == Task.ACTION_BUILD:
            command += actions.get(action).split(' ')
        else:
            command += ['run', '--rm']
            command += [f'-e {var.name}={var.value}' for var in project.system_variables.all()]
            command += ['app', 'cap']
            command.append(stage.name)
            command.append(actions.get(action))

        return ' '.join(command)

    @staticmethod
    def __validate_action(action: str):
        if action not in Task.ACTIONS:
            raise Exception(f'Action "{action}" id invalid.')
