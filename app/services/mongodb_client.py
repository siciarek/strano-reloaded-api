import pymongo
from bson.objectid import ObjectId


class MongoDbClient:

    def __init__(self, host: str, username: str, password: str, default_database: str, default_collection: str):
        self.host = host
        self.client = pymongo.MongoClient(host=self.host, username=username, password=password)
        self.default_database = self.validate_database(default_database)
        self.default_collection = default_collection

    def validate_database(self, database: str):
        if database is not None and database not in self.available_databases:
            raise Exception(f'Database "{database}" is not available for host "{self.host}"')

        return database

    @property
    def available_databases(self):
        return self.client.list_database_names()

    def remove_one(self, id: str, collection: str = None, database: str = None):
        return self.__get_collection(database=database, collection=collection).delete_one({
            "_id": ObjectId(id),
        }).raw_result

    def insert(self, row: dict, collection: str = None, database: str = None):
        return self.__get_collection(database=database, collection=collection).insert(row)

    def search(self, query: dict, collection: str = None, database: str = None):
        return self.__get_collection(database=database, collection=collection).find(query)

    def __get_collection(self, database: str, collection: str):
        database = self.validate_database(database)
        database = self.client[self.default_database] if database is None else self.client[database]
        return database[self.default_collection] if collection is None else database[collection]
