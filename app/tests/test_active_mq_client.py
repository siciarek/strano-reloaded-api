from django.test import TransactionTestCase
from unittest_data_provider import data_provider
from unittest import mock

from app.container import Ioc
from app.services import ActiveMqClient


class CampaignNotifierTestCase(TransactionTestCase):
    srv: ActiveMqClient

    def setUp(self) -> None:
        self.srv = Ioc.active_mq_client()

    def test_container_check(self):
        self.assertEqual(type(self.srv), ActiveMqClient)
