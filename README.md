# Strano Reloaded

Webistrano like web client API for `Capistrano v3`.

## Environment configuration

You should have `docker` and `docker-compose` apps available.

## Set up

Run the following commands.

```bash
docker-compose install
```
