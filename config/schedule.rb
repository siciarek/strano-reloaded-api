app_root_directory = File.expand_path("../../../../current", __FILE__)
chdir = "cd #{app_root_directory}"
cmd = "/home/www/.pyenv/shims/pipenv run ./manage.py"
filter = "2>&1 | grep -v 'Loading .env environment variables…'"

set :chronic_options, hours24: true
set :job_template, nil
env 'MAILTO', 'siciarek@gmail.com'

# https://github.com/javan/whenever

# Do the job every minute.
every 1.minute do

end
