from django.contrib.auth import get_user_model
from django.test import Client
from django.test import TransactionTestCase
from django.utils.http import urlencode
from rest_framework import status as http_status
from unittest_data_provider import data_provider

from app.models import Task


class ApiTestCase(TransactionTestCase):
    fixtures = ('auth', 'projecttemplate', 'project', 'stage', 'systemvariable', 'projectconfigurationvalue',
                'host', 'stagehostrole', 'recipe', 'stagerecipe',)

    USERNAME: str = 'colak'
    EMAIL: str = 'colak@example.com'
    PASS: str = 'helloworld'

    def setUp(self):
        user = get_user_model()
        user.objects.create_superuser(self.USERNAME, self.EMAIL, self.PASS)
        self.c = Client()

    @data_provider(lambda: (
            (0, 1, 1, Task.ACTION_INFO, None),
            (1, 1, 1, Task.ACTION_SETUP, None),
            (2, 1, 1, Task.ACTION_CHECK, None),
            (3, 1, 1, Task.ACTION_BUILD, None),
            (4, 1, 1, Task.ACTION_DEPLOY, None),
            (5, 1, 1, Task.ACTION_EXECUTE, {"task": "doctor:variables"}),
    ))
    def test_runner_view_set_create(self, count, project_id, stage_id, action, url_params):
        self.assertEqual(Task.objects.count(), count)

        path = f'/project/{project_id}/stage/{stage_id}/{action}/'

        if type(url_params) is dict:
            path += f'?{urlencode(url_params)}'

        response = self.c.post(path, **self.__get_auth_headers())
        self.assertEqual(response.status_code, http_status.HTTP_200_OK, response.json())

        data = response.json()
        self.assertEqual(dict, type(data))
        self.assertTrue('id' in data.keys())
        self.assertEqual(Task.objects.count(), count + 1)

        task = Task.objects.all().order_by('-id').first()
        self.assertEqual(task.action, action)

    def test_log_in_successfully(self):
        response = self.c.post('/auth', {'username': self.USERNAME, 'password': self.PASS})
        self.assertEqual(http_status.HTTP_200_OK, response.status_code)
        data = response.json()
        self.assertEqual(type(data).__name__, 'dict')
        self.assertTrue('token' in data)

    def test_log_in_unsuccessfully(self):
        from django.utils.translation import gettext as _
        response = self.c.post('/auth', {'username': self.USERNAME, 'password': 'drakarys'})
        self.assertEqual(http_status.HTTP_400_BAD_REQUEST, response.status_code)
        data = response.json()
        self.assertEqual(type(data).__name__, 'dict')
        self.assertFalse('token' in data)
        self.assertDictEqual({"non_field_errors": [_("Unable to log in with provided credentials.")]}, data)

    def __get_auth_headers(self):
        response = self.c.post('/auth', {'username': self.USERNAME, 'password': self.PASS})
        token = response.json().get('token')

        return {
            'HTTP_AUTHORIZATION': f'Bearer {token}',
            'content_type': 'application/json',
        }
