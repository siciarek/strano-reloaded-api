import json
from datetime import datetime

from crequest.middleware import CrequestMiddleware
from django.contrib.auth.models import User
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from slugify import slugify

from app.models import (
    SYSTEM_USER_ID,
    Project,
    Task,
)


@receiver(pre_save, sender=Project)
def project_pre_save(sender, instance, **kwargs):
    if True or instance.id is None:
        instance.directory_name = slugify(instance.name)


@receiver(post_save, sender=Task)
def task_post_cancel(sender, instance, created, **kwargs):
    if instance.status == Task.STATUS_CANCELED:
        request = CrequestMiddleware.get_request()
        user = User.objects.get(id=SYSTEM_USER_ID) if request is None else request.user

        with open("task.json", "w") as out:
            out.write(json.dumps(
                obj=dict(
                    data=str(user),
                    datatime=datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                    status=instance.status,
                ),
                indent=4,
            ))
