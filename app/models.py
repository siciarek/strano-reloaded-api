import jsonfield
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel, TitleDescriptionModel
from django.contrib.auth.models import User as AuthUser
from django_mysql.models import ListCharField
from simple_history.models import HistoricalRecords

SYSTEM_USER_ID = 1


class UniqueNameDescriptionModel(models.Model):
    """
    UniqueNameDescriptionModel

    An abstract base class model that provides unique name and description fields.
    """

    name = models.CharField(_('name'), unique=True, max_length=255)
    description = models.TextField(_('description'), blank=True, null=True)

    class Meta:
        abstract = True


class SystemVariable(TimeStampedModel):
    project = models.ForeignKey(to='Project', on_delete=models.CASCADE, blank=True, related_name='system_variables')
    stage = models.ForeignKey(to='Stage', on_delete=models.CASCADE, blank=True, null=True,
                              related_name='system_variables')
    name = models.CharField(_('name'), max_length=255)
    value = models.CharField(_('value'), max_length=255)

    def __str__(self):
        return f'{self.project}/{self.stage}/{self.name}'

    class Meta:
        db_table = 'app_system_variable'
        ordering = ('-stage', 'project', 'name',)


class Recipe(UniqueNameDescriptionModel):
    body = models.TextField(_('body'))
    creator = models.ForeignKey(to=AuthUser, on_delete=models.CASCADE, related_name='recipes')

    def __str__(self):
        return f'{self.name}'

    class Meta:
        ordering = ('name',)


class ConfigurationValue(TimeStampedModel):
    name = models.CharField(_('name'), max_length=255)
    value = models.CharField(_('value'), blank=True, null=True, max_length=255)
    prompt_on_deploy = models.BooleanField(_('prompt_on_deploy'), default=False)

    def __str__(self):
        return f'{self.name}={self.value}'

    class Meta:
        abstract = True


class Host(TimeStampedModel):
    address = models.CharField(_('address'), max_length=255)

    def __str__(self):
        return f'{self.address}'

    class Meta:
        db_table = 'app_host'
        ordering = ('-id',)


class StageHostRole(TimeStampedModel):
    roles = (
        ('app', 'app',),
        ('db', 'db',),
        ('web', 'web',),
    )
    stage = models.ForeignKey('Stage', on_delete=models.CASCADE, related_name='stages')
    role = models.CharField(_('role'), blank=True, choices=roles, max_length=8)
    custom_role = models.CharField(_('custom_role'), blank=True, max_length=64)
    host = models.ForeignKey('Host', on_delete=models.CASCADE, related_name='roles')
    ssh_port = models.PositiveSmallIntegerField(_('ssh_port'), blank=True, null=True)
    primary = models.BooleanField(_('primary'), default=False)
    no_release = models.BooleanField(_('no_release'), default=False)
    no_symlink = models.BooleanField(_('no_symlink'), default=False)

    def __str__(self):
        return f'{self.stage}/{self.host}/{self.role}{self.custom_role}'

    class Meta:
        db_table = 'app_stage_host_role'


class ProjectConfigurationValue(ConfigurationValue):
    project = models.ForeignKey('Project', on_delete=models.CASCADE, related_name='configuration_values')

    def __str__(self):
        return f'{self.project}: {self.name}={self.value}'

    class Meta:
        db_table = 'app_project_configuration_value'
        ordering = ('name',)


class StageConfigurationValue(ConfigurationValue):
    stage = models.ForeignKey('Stage', on_delete=models.CASCADE, related_name='configuration_values')

    def __str__(self):
        return f'{self.stage}: {self.name}={self.value}'

    class Meta:
        db_table = 'app_stage_configuration_value'
        ordering = ('name',)


class ProjectTemplate(TimeStampedModel, UniqueNameDescriptionModel):
    DIR_TEMPLATE_PLAIN = 'plain'
    DIR_TEMPLATE_DJANGO = 'django'
    DIR_TEMPLATE_SYMFONY3 = 'symfony3'
    DIR_TEMPLATES = (
        DIR_TEMPLATE_PLAIN,
        DIR_TEMPLATE_DJANGO,
        DIR_TEMPLATE_SYMFONY3,
    )

    dir_template = models.CharField(
        verbose_name=_('dir_template'),
        choices=((t, t) for t in DIR_TEMPLATES),
        default=DIR_TEMPLATE_PLAIN,
        max_length=32,
    )

    def __str__(self):
        return f'{self.name}'

    class Meta:
        db_table = 'app_project_template'
        ordering = ('-id',)


class Project(TimeStampedModel, UniqueNameDescriptionModel):
    history = HistoricalRecords()
    template = models.ForeignKey(ProjectTemplate, on_delete=models.CASCADE, related_name='projects')
    directory_name = models.CharField(_('directory_name'), unique=True, max_length=255)
    hooks = models.TextField(_('hooks'), blank=True, null=True)

    def __str__(self):
        return f'{self.name}'

    class Meta:
        db_table = 'app_project'
        ordering = ('-id',)


class Stage(TimeStampedModel):
    history = HistoricalRecords()
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name='stages')
    name = models.CharField(_('name'), max_length=255)
    description = models.TextField(_('description'), blank=True, null=True)
    hooks = models.TextField(_('hooks'), blank=True, null=True)
    notification_emails = ListCharField(
        base_field=models.CharField(max_length=128),
        max_length=1024,
    )

    def __str__(self):
        return f'{self.project}/{self.name}'

    class Meta:
        db_table = 'app_stage'
        ordering = ('-id',)


class StageRecipe(TimeStampedModel):
    recipe = models.ForeignKey(to=Recipe, on_delete=models.CASCADE, related_name='stage_recipes')
    stage = models.ForeignKey(to=Stage, on_delete=models.CASCADE, related_name='stage_recipes')

    def __str__(self):
        return f'{self.stage} - {self.recipe}'

    class Meta:
        db_table = 'app_stage_recipe'
        ordering = ('-id',)


class Task(TimeStampedModel):
    ACTION_BUILD = 'build'
    ACTION_INFO = 'info'
    ACTION_DEPLOY = 'deploy'
    ACTION_CHECK = 'check'
    ACTION_SETUP = 'setup'
    ACTION_EXECUTE = 'execute'
    ACTIONS = (
        ACTION_BUILD,
        ACTION_INFO,
        ACTION_DEPLOY,
        ACTION_CHECK,
        ACTION_SETUP,
        ACTION_EXECUTE,
    )
    STATUS_PENDING = 'pending'
    STATUS_RUNNING = 'running'
    STATUS_FULFILLED = 'fulfilled'
    STATUS_FAILED = 'failed'
    STATUS_CANCELED = 'canceled'
    STATUSES = (
        STATUS_PENDING,
        STATUS_RUNNING,
        STATUS_FULFILLED,
        STATUS_FAILED,
        STATUS_CANCELED,
    )

    status = models.CharField(_('status'), choices=((s, s,) for s in STATUSES), default=STATUS_PENDING, max_length=16)
    pid = models.IntegerField(_('pid'), blank=True, null=True)
    action = models.CharField(_('action'), max_length=255)
    command = models.CharField(_('command'), max_length=1024)
    working_directory = models.CharField(_('working_directory'), max_length=1024)
    project = models.ForeignKey(to=Project, on_delete=models.CASCADE, related_name='tasks')
    stage = models.ForeignKey(to=Stage, on_delete=models.CASCADE, related_name='tasks')
    revision = models.CharField(_('revision'), blank=True, null=True, max_length=255)
    description = models.TextField(_('description'), blank=True, max_length=512)
    run_by = models.ForeignKey(to=AuthUser, on_delete=models.CASCADE, related_name='tasks')
    data = jsonfield.JSONField(default=[])

    def __str__(self):
        return f'{self.id}/{self.action}/{self.status}'

    class Meta:
        db_table = 'app_task'
        ordering = ('-id',)
