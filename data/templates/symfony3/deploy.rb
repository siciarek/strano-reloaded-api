# config valid for current version and patch releases of Capistrano
lock "~> 3.12.0"

set :format, ENV["OUTPUT_FORMAT"] || "airbrussh" # "airbrussh", "simpletext", "blackhole", "dot", "pretty"
set :application, ENV["APPLICATION"] || "application"
set :branch, ENV["BRANCH"] || "master"
set :local_user, ENV["LOCAL_USER"] || "admin"
set :repo_url, ENV["REPO_URL"]
set :deploy_to, ENV["DEPLOY_TO"] || "/data/www"
set :keep_releases, ENV["KEEP_RELEASES"] || 1
set :whenever_command, ENV["WHENEVER_COMMAND"] || "whenever"

set :linked_dirs, %w[ var/logs var/sessions app/data/invoices app/documents ]
set :linked_files, %w[ app/config/parameters.yml ]
