import django.conf.urls
from rest_framework import routers
from django.conf.urls import include
from django.urls import path

from . import views

router = routers.DefaultRouter()
router.register(
    r'project/(?P<project>[1-9]\d*)/stage/(?P<stage>[1-9]\d*)/(?P<action>build|deploy|check|setup|execute|info)',
    views.TaskRunnerViewSet,
    basename='runner',
)
router.register(r'project-template', views.ProjectTemplateViewSet)
router.register(r'stage-host-role', views.StageHostRoleViewSet)
router.register(r'stage-recipe', views.StageRecipeViewSet)
router.register(r'task', views.TaskViewSet)
router.register(r'project-configuration-value', views.ProjectConfigurationValueViewSet)
router.register(r'host', views.HostViewSet)
router.register(r'recipe', views.RecipeViewSet)
router.register(r'project', views.ProjectViewSet)
router.register(r'stage', views.StageViewSet)

urlpatterns = [
    path(r'', views.home, name='home'),
    django.conf.urls.url(r'^', include(router.urls,)),
]
