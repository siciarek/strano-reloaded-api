from django.apps import AppConfig


class LoadReceivers(AppConfig):
    name = "app"

    def ready(self):
        from .signal_receivers import task_post_cancel, project_pre_save
