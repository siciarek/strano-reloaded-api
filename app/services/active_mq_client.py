import json
import time

from stomp import Connection, ConnectionListener


class MessageListener(ConnectionListener):
    messages = []

    def on_message(self, headers, body):
        """
        :param dict headers:
        :param body:
        """
        self.messages.append(body)

    def on_error(self, headers, body):
        """
        :param dict headers:
        :param body:
        """
        print('on_error %s %s' % (headers, body))


class ActiveMqClient:
    conn: Connection

    def __init__(self, logger, config, default_queue, disconnect_delay_secs):
        self.logger = logger
        self.default_queue = default_queue
        self.username = config.username
        self.password = config.password
        self.host = config.hostname
        self.port = config.port
        self.listener = MessageListener()
        self.disconnect_delay_secs = disconnect_delay_secs

    def __connect(self, queue: str):
        destination = queue if queue is not None else self.default_queue
        conn = Connection(host_and_ports=[(self.host, self.port,), ])
        conn.set_listener(name='', lstnr=self.listener)
        conn.start()
        conn.connect(self.username, self.password, wait=True)
        return conn, destination

    def __disconnect(self, conn: Connection):
        time.sleep(self.disconnect_delay_secs)
        conn.disconnect()

    def read(self, queue: str = None) -> list:
        conn, destination = self.__connect(queue)

        conn.subscribe(
            destination=destination,
            id='1',
        )

        self.__disconnect(conn)

        result = [json.loads(msg) for msg in self.listener.messages]
        self.listener.messages = []

        return result

    def write(self, data, queue: str = None, format: bool = False):
        conn, destination = self.__connect(queue)

        body = json.dumps(data) if format is False else json.dumps(data, indent=2)

        conn.send(
            destination=destination,
            body=body,
            headers={'persistent': 'true'},
        )

        self.__disconnect(conn)
