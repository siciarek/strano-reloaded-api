from api.serializer import UserSerializer
from libgravatar import Gravatar


def response_payload_handler(token, user=None, request=None):
    data = UserSerializer(user, context={'request': request}).data
    data['gravatar'] = Gravatar(email=data.get('email')).get_image()

    return {
        'token': token,
        'data': data
    }
