from django.shortcuts import redirect
from rest_framework import viewsets, status as http_status
from rest_framework.exceptions import NotAcceptable, NotFound
from rest_framework.response import Response

from api.serializer import *
from app.container import Ioc
from app.models import *


def wrap_in_frame(results: list):
    return dict(
        count=len(results),
        next=None,
        previous=None,
        results=results,
    )


def home(request):
    response = redirect('/admin/')
    return response


class TaskRunnerViewSet(viewsets.ViewSet):

    @staticmethod
    def create(request, project, stage, action) -> Response:
        srv = Ioc.task_runner()

        try:
            project_entity = Project.objects.get(id=project)
            stage_entity = Stage.objects.get(id=stage)
            if stage_entity.project != project_entity:
                raise NotAcceptable('Invalid project or stage data.')

            task = srv.create(
                project=project_entity,
                stage=stage_entity,
                action=action,
                capistrano_task=request.GET.get('task', ''),
                creator=request.user,
            )

            serializer = TaskSerializer(task)

        except Exception as e:
            raise NotFound(str(e))

        return Response(data=serializer.data, status=http_status.HTTP_200_OK)


class StageHostRoleViewSet(viewsets.ModelViewSet):
    queryset = StageHostRole.objects.all()
    serializer_class = StageHostRoleSerializer


class ProjectTemplateViewSet(viewsets.ModelViewSet):
    queryset = ProjectTemplate.objects.all()
    serializer_class = ProjectTemplateSerializer


class StageRecipeViewSet(viewsets.ModelViewSet):
    queryset = StageRecipe.objects.all()
    serializer_class = StageRecipetSerializer


class HostViewSet(viewsets.ModelViewSet):
    queryset = Host.objects.all()
    serializer_class = HostSerializer


class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer


class ProjectConfigurationValueViewSet(viewsets.ModelViewSet):
    queryset = ProjectConfigurationValue.objects.all()
    serializer_class = ProjectConfigurationValueSerializer


class RecipeViewSet(viewsets.ModelViewSet):
    queryset = Recipe.objects.all()
    serializer_class = RecipeSerializer


class ProjectViewSet(viewsets.ModelViewSet):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer


class StageViewSet(viewsets.ModelViewSet):
    queryset = Stage.objects.all()
    serializer_class = StageSerializer
